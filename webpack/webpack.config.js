var webpack = require('webpack'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    autoprefixer = require('autoprefixer'),
    rupture = require('rupture'),
    jeet = require('jeet');


var STATIC_PATH = __dirname + '/../static';


module.exports = {
    entry: {
        front: STATIC_PATH + '/apps/front/js/app.js',
        // back
        // blog
    },
    output: {
        filename: STATIC_PATH + '/apps/[name]/js/all.js'
    },
    module: {
        loaders: [
            {
                test: /\.styl$/i,
                loader: ExtractTextPlugin.extract('raw-loader!postcss-loader!stylus-loader')

            },
            {
                test: /\.css$/i,
                loader: ExtractTextPlugin.extract('raw-loader!postcss-loader')
            }
        ],

        ////insert raw code into webpack bundle, without perform loaders ops
        //noParse: [
        //    /static\/vendor.+?\.js$/,
        //    /static\/lib.+?\.js$/,
        //]
    },
    resolve: {
        root: [
            STATIC_PATH,

        ],
        extensions: ['', '.js'],  //require('module.js') or require('module') for js

        // most recent libs put into alias
        alias: {
            jquery: STATIC_PATH + '/vendor/js/jquery.js'
        },
    },

    resolveLoader: {
        // bug cant resolve defaults paths
        modulesDirectories: [
            __dirname + '/node_modules',
        ]
    },
    plugins: [
        // we need separated all.css file, on default - webpack place css in html code
        new ExtractTextPlugin('../static/apps/[name]/css/all.css'),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            //React:'react'
            //'window.socket': 'socket',
            //'w'io: 'io',
            //'io' : 'io',
            //'window.io': 'io'
        }),
    ],

    postcss: function () {
        return [autoprefixer({browsers: ['last 10 versions']})] // last 10 versions only for prefix test
    },

    stylus: {
        use: [jeet(), rupture()], // jeet - universal grid system
    }

};
