var express = require('express'),
    nunjucks = require('nunjucks'),
    RXS = require('./tags_common'),
    app = express(),
    path = require('path');

console.log(__dirname);
app.use('/static', express.static(path.join(__dirname, '/../static')));

var env = nunjucks.configure('../templates', {
    autoescape: true,
    express: app
});

env.addExtension('tag_rxs', new RXS.tag_rxs());

app.get('/', function (req, res) {
    // here we have test template
    res.render('apps/front/index.j2');
});


var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
});