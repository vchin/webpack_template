// styles
require('../styl/app.styl')
// require('../css/my.css')

// vendor libraries
// expose - import into global context as jQuery var
require('expose?jQuery!jquery')

// project libs
require('expose?_inline_init!lib/js/inline_js.js');

// my code
//require('../js/my_code.js')

